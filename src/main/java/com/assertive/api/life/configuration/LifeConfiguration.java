package com.assertive.api.life.configuration;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan("com.assertive.api.life")
@EnableJpaRepositories("com.assertive.api.life")
@EnableAsync
@EnableTransactionManagement
public class LifeConfiguration {
}
